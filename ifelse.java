class ifElse
{
	public static void main(String[] args)
	{
		int a,b;
		b = 10;

		for(a = 0; a<=11; a++)
		{
			if(a < b)
				System.out.println("a = " + a + ", b = 10, that means a < b is correct.");
			else if (a == b)
				System.out.println("a = " + a + ", b = 10, that means a = b is correct.");
			else
				System.out.println("a = " + a + ", b = 10, that means a > b is correct.");
		}
	}
}
